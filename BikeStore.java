// Matteo Robidoux //
// 1934997 //

public class BikeStore 
{
    public static void main(String[]args) 
    {
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("Tommy", 4, 51.53);
        bikes[1] = new Bicycle("Rolly", 2, 33.62);
        bikes[2] = new Bicycle("Biky", 6, 61.25);
        bikes[3] = new Bicycle("Speed", 8, 65.92);

        for(int i = 0; i < bikes.length; i++)
        {
            System.out.println(bikes[i]);
        }
    }
}
